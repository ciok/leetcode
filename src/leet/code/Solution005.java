package leet.code;

/**
 * 
 * @author alexandru.ciouca
 *
 *         5. Longest Palindromic Substrin
 *
 */
public class Solution005 {

	public static void main(final String[] args) {
		System.out.println(longestPalindrome("a"));
		System.out.println(longestPalindrome("bb"));
		System.out.println(longestPalindrome("ababbbb"));
		System.out.println(longestPalindrome("babad"));
	}

	public static String longestPalindrome(String s) {
		int start = 0, end = 0;
		if (s.length() < 2) {
			return s;
		}
		char[] a = s.toCharArray();
		for (int i = 0; i < a.length - 1; i++) {
			int len1 = expAtCenter(a, i, i);
			int len2 = expAtCenter(a, i, i + 1);
			int len = Math.max(len1, len2);
			if (end - start < len) {
				start = i - (len - 1) / 2;
				end = i + (len / 2) + 1;
			}
		}
		return s.substring(start, end);
	}

	private static int expAtCenter(char[] a, int left, int right) {
		while (left >= 0 && right < a.length && a[left] == a[right]) {
			left--;
			right++;
		}
		return right - left - 1;
	}
}
