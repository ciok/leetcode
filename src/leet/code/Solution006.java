package leet.code;

/**
 * 
 * @author alexandru.ciouca
 * 
 *         6. ZigZag Conversion
 *
 */
public class Solution006 {

	// TODO
	public static void main(final String[] args) {
		System.out.println(convert("AAAAAA", 2));
		System.out.println(convert("A  \n S \n  D", 3));
	}

	public static String convert(String s, int numRows) {
		if (s.isEmpty()) {
			return s;
		}
		final int rowLen = s.length() / numRows - (s.charAt(s.length() - 1) == '\n' ? 1 : 0);
		final char[] ret = new char[rowLen * numRows];
		for (int i = 0, j = 0, k = 0; i < s.length(); i++) {
			if (s.charAt(i) == '\n') {
				j++;
				k = 0;
				continue;
			}

			if (s.charAt(i) == ' ') {
				continue;
			}

			ret[rowLen * k + j] = s.charAt(i);
			k++;
		}
		return new String(ret);
	}
}
