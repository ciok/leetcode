package leet.code;

/**
 * 
 * @author alexandru.ciouca
 * 
 *         8. String to Integer (atoi)
 *
 */
public class Solution008 {

	public static void main(final String[] args) {
		System.out.println(myAtoi("+-2"));
		System.out.println(myAtoi("++1"));
		System.out.println(myAtoi("   +0 123"));
		System.out.println(myAtoi("2147483648"));
		System.out.println(myAtoi(String.valueOf(Integer.MIN_VALUE)));
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE);
	}

	public static int myAtoi(String s) {
		s = s.trim();
		if (s.isEmpty()) {
			return 0;
		}
		int si = 0;
		char c = s.charAt(si);
		int sign = 1;
		if (c == '-') {
			sign = -1;
			si++;
		}
		if (c == '+') {
			si++;
		}
		long nr = 0;
		for (int i = si; i < s.length() && s.charAt(i) >= '0' && s.charAt(i) <= '9'; i++) {
			if (nr > Integer.MAX_VALUE / 10 || (nr >= Integer.MAX_VALUE / 10 && s.charAt(i) >= '8')) {
				return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
			}
			nr = nr * 10 + (s.charAt(i) - '0');
		}
		return (int) nr * sign;
	}
}
