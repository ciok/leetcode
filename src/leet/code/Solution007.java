package leet.code;

/**
 * 
 * @author alexandru.ciouca
 *
 *         7. Reverse Integer
 * 
 */
public class Solution007 {

	public static void main(final String[] args) {
		System.out.println(reverse(123));
		System.out.println(reverse(-123));
		System.out.println(reverse(120));
	}

	public static int reverse(int x) {
		long r = 0;
		while (x != 0) {
			r = r * 10 + x % 10;
			x = x / 10;
		}
		if (r >= Integer.MAX_VALUE || r <= Integer.MIN_VALUE) {
			return 0;
		}
		return (int) r;
	}
}
