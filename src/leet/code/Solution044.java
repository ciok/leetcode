package leet.code;

/**
 * 
 * @author alexandru.ciouca
 * 
 *         44. Wildcard Matching
 *
 */
public class Solution044 {

	// TODO

	public static boolean isMatch(String s, String p) {
		if (p.isEmpty()) {
			return s.isEmpty();
		}
		if (s.isEmpty()) {
			return p.charAt(0) == '*' && isMatch(s, p.substring(1));
		}
		boolean pass1 = !s.isEmpty() && (s.charAt(0) == p.charAt(0) || p.charAt(0) == '?');
		if (p.charAt(0) == '*') {
			int pos = 0;
			for (int i = 1; i < p.length(); i++) {
				if (p.charAt(i) != '*') {
					pos = i;
					break;
				}
			}
			return isMatch(s.substring(1), p.substring(pos)) || isMatch(s, p.substring(pos));
		} else {
			return pass1 && isMatch(s.substring(1), p.substring(1));
		}
	}

	public static void main(String[] args) {
		System.out.println(isMatch("aa", "*")); // true
		System.out.println(isMatch("aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba", "a*******b")); // false
		System.out.println(
				isMatch("babbbbaabababaabbababaababaabbaabababbaaababbababaaaaaabbabaaaabababbabbababbbaaaababbbabbbbbbbbbbaabbb",
						"b**bb**a**bba*b**a*bbb**aba***babbb*aa****aabb*bbb***a")); // false
		System.out.println(isMatch("abefcdgiescdfimde", "ab*cd?i*de")); // true
	}
}
