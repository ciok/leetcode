package leet.code;

/**
 * 
 * @author alexandru.ciouca
 *
 * 2. Add Two Numbers
 *
 */
public class Solution002 {

	public static void main(String[] args) {
		// addTwoNumbers
	}

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode result = new ListNode(0), last = result;
		int sum = 0;

		while (l1 != null || l2 != null) {
			if (l1 != null) {
				sum += l1.val;
				l1 = l1.next;
			}
			if (l2 != null) {
				sum += l2.val;
				l2 = l2.next;
			}
			last.next = new ListNode(sum % 10);
			last = last.next;
			sum = sum / 10;
		}

		if (sum > 0) {
			last.next = new ListNode(sum);
		}

		return result.next;
	}

	private class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}
	}
}
