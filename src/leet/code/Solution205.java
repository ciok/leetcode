package leet.code;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alexandru.ciouca
 *
 * 205. Isomorphic Strings
 *
 */
public class Solution205 {

	public static void main(String[] args) {
		System.out.println(isIsomorphic("aa", "ab"));
		System.out.println(isIsomorphic("ab", "aa"));
		System.out.println(isIsomorphic("aab", "aaa"));
		System.out.println(isIsomorphic("add", "egg"));
		System.out.println(isIsomorphic("paper", "title"));
	}

	public static boolean isIsomorphic(String s, String t) {
		if (s.equals(t)) {
			return true;
		}

		final char[] map = new char[256];
		final boolean[] used = new boolean[256];
		final char[] ss = s.toCharArray();
		final char[] tt = t.toCharArray();

		for (int i = 0; i < ss.length; i++) {
			if (map[ss[i]] == 0) {
				if (used[tt[i]]) {
					return false;
				}
				map[ss[i]] = tt[i];
				used[tt[i]] = true;
			} else if (map[ss[i]] != tt[i]) {
				return false;
			}
		}
		return true;
	}

	public static boolean isIsomorphic2(String s, String t) {
        final Map<Character, Character> map = new HashMap<>();
        final char[] ss = s.toCharArray();
        char[] tt = t.toCharArray();
        for (int i = 0; i < ss.length; i++) {
            if (map.containsKey(tt[i])) {
                if (map.get(tt[i]).equals(ss[i])) {
                	continue;
                }
                return false;
            }
            if (map.containsValue(ss[i])) {
            	return false;
            }
            map.put(tt[i], ss[i]);
        }
        return true;
    }
}
