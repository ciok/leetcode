package leet.code;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author alexandru.ciouca
 * 
 * 13. Roman to Integer
 *
 */
public class Solution013 {

	final static Map<Character, Integer> map = new HashMap<>();
	static {
		map.put('I', 1);
		map.put('V', 5);
		map.put('X', 10);
		map.put('L', 50);
		map.put('C', 100);
		map.put('D', 500);
		map.put('M', 1000);
	}

	public static int romanToInt(String s) {
		final char[] ss = s.toCharArray();
		int prev = 0, sum = 0;
		for (int i = 0; i < ss.length; i++) {
			int val = map.get(ss[i]);
			if (prev < val) {
				val -= 2 * prev;
			}
			sum += val;
			prev = val;
		}
		return sum;
	}

	public static void main(String[] args) {
		System.out.println(romanToInt("XLV"));
	}
}
