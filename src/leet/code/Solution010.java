package leet.code;

/**
 * 
 * @author alexandru.ciouca
 * 
 * 10. Regular Expression Matching
 *
 */
public class Solution010 {

	public static boolean isMatch(String s, String p) {
		if (p.isEmpty()) {
			return s.isEmpty();
		}
		boolean pass = !s.isEmpty() && (s.charAt(0) == p.charAt(0) || p.charAt(0) == '?');
		if (p.length() > 1 && p.charAt(1) == '*') {
			return isMatch(s, p.substring(2)) || (pass && isMatch(s.substring(1), p));
		} else {
			return pass && isMatch(s.substring(1), p.substring(1));
		}
	}

	public static void main(String[] args) {
		System.out.println(isMatch("aaa", "a*a"));
	}
}
