package leet.code;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author alexandru.ciouca
 *
 *         3. Longest Substring Without Repeating Characters
 *
 */
public class Solution003 {

	public static void main(String[] args) {
		System.out.println(lengthOfLongestSubstring("abcabcbb"));
		System.out.println(lengthOfLongestSubstring("bbbb"));
		System.out.println(lengthOfLongestSubstring("pwwkew"));
		System.out.println(lengthOfLongestSubstring("dvdf"));
		System.out.println(lengthOfLongestSubstring("abba"));
	}

	public static int lengthOfLongestSubstring(String s) {
		char[] cs = s.toCharArray();
		if (cs.length < 2) {
			return cs.length;
		}
		int max = 0, len = 0;
		for (int i = 1, j = 0, mid = 0; i < cs.length; i++) {
			for (j = mid; j < i; j++) {
				if (cs[i] == cs[j]) {
					break;
				}
			}
			if (j < i) {
				mid = j + 1;
			}
			len = i - mid + 1;

			if (len > max) {
				max = len;
			}
		}
		return max;
	}

	public static int lengthOfLongestSubstring3(String s) {
		int ans = 0;
		Map<Character, Integer> map = new HashMap<>();
		for (int i = 0, j = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (map.containsKey(c)) {
				j = Math.max(map.get(c) + 1, j);
			}
			map.put(c, i);
			ans = Math.max(ans, i - j + 1);
		}
        return ans;
	}

	public static int lengthOfLongestSubstring2(String s) {
		int n = s.length();
        Set<Character> set = new HashSet<>();
        int ans = 0, i = 0, j = 0;
        while (i < n && j < n) {
            // try to extend the range [i, j]
            if (!set.contains(s.charAt(j))){
                set.add(s.charAt(j++));
                ans = Math.max(ans, j - i);
            }
            else {
                set.remove(s.charAt(i++));
            }
        }
        return ans;
	}
}
