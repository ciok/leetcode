package leet.code;

import java.util.HashMap;
import java.util.Map;

import leet.code.utils.Utils;

/**
 * 
 * @author alexandru.ciouca
 *
 * 1. Two Sum
 * 
 */
public class Solution001 {

	public static void main(String[] args) {
		final int[] nums = {11, 7, 2, 15};
		final int target = 13;

		Utils.print(twoSum(nums, target));
	}
	
	public static int[] twoSum(int[] nums, int target) {
		final int[] result = new int[2];
		final Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for (int i = 0; i < nums.length; i++) {
			if (map.containsKey(target - nums[i])) {
				result[0] = map.get(target - nums[i]);
				result[1] = i;
				return result;
			}
			map.put(nums[i], i);
		}

        return result;
    }
}
